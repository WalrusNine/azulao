'use strict';

var userl = angular.module('azulao.userlist', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'xeditable', 'zumba.angular-waypoints', 'auth', 'data']);

userl.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/userlist', {
    templateUrl: 'angular/userlist/userlist.html',
    controller: 'UserListCtrl'
  });
}]);

userl.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});

userl.controller('UserListCtrl', ['$scope', 'AuthService', 'DataService', '$location', function(sc, AuthService, DataService, loc) {
    sc.userlist = null;
    DataService.getUsers().then( function(result) {
        if (result != null) {
            sc.userlist = result;
        }
        else {
            // Error loading user list
        }
    });
	sc.searchUser = "";

	sc.goToUserPage = function (user) {
		if (user == AuthService.currentUser())
			loc.url('/perfil');
		else loc.url('/user/' + user.username);
	};
}]);
