/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
'use strict'

const Promise = require('bluebird')

module.exports = {
	create: (req, res, next) => {

		const params = req.params.all()

		Promise.all([
			TagService.createTags(params.text, params.createdBy),
			Post.create(params)
		])
		.spread((tags, post) => {
			post.tags.add(tags)
			post.save()
			return res.json(post)
		})
		.catch(error => next(error))

	}
}
