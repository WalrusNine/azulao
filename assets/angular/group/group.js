'use strict';

var group = angular.module('azulao.group', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'xeditable', 'zumba.angular-waypoints', 'auth', 'data', 'lazy-scroll']);

group.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/group', {
		templateUrl: 'angular/group/group.html',
		controller: 'GroupCtrl'
	});
	$routeProvider.when('/group/:group_id', {
		templateUrl: 'angular/group/group.html',
		controller: 'GroupCtrl'
	});
}]);

group.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});

group.controller('GroupInfoCtrl', ['$scope', 'AuthService', 'DataService', '$uibModal', function(sc, AuthService, DataService, modal) {
	// TODO: if is owner can edit
}]);

group.controller('GroupPostCtrl', ['$scope', 'AuthService', 'DataService', function(sc, AuthService, DataService) {
	sc.tempPost = new Post();

	sc.addPost = function() {
		if (sc.isMember === true) {
			// Create post
			var p = DataService.createPost(AuthService.currentUser(), sc.tempPost.title, sc.tempPost.content, sc.currentGroup);

			// Add post
			DataService.addPost(p);

			// Reset dummy variable
			sc.tempPost.title = "";
			sc.tempPost.content = "";
		}
	};

}]);

group.controller('GroupPostListCtrl', ['$scope', 'AuthService', 'DataService', function(sc, AuthService, DataService) {
	// Posts currently showing to user
    sc.posts = [];
    sc.postsIndex = 0;

    // Gets n posts
    sc.getPost = function(n) {
		if (sc.currentGroup != null) {
			// sc.posts is a subarray of sc.currentUser.postlist
			sc.postsIndex += n;

			if (sc.postsIndex > sc.currentGroup.postlist.length)
				sc.postsIndex = sc.currentGroup.postlist.length;

			sc.posts = sc.currentGroup.postlist.slice(0, sc.postsIndex);
		}
    };

	// Get initial posts
	sc.getPost(5);

	// If group receives new post, update
	sc.$watchCollection("currentGroup.postlist", function(newValue, oldValue) {
		sc.getPost(1);
	});

    sc.reachEndEvent = function() {
		sc.getPost(5);
	};

	sc.goToUserPage = function(user) {
		console.log("Go to " + user.username);
		if (user != sc.listOwner)
			loc.url('/user/' + user.username);
	};

}]);

group.controller('GroupCtrl', ['$scope', 'AuthService', 'DataService', '$location', '$routeParams', function(sc, AuthService, DataService, loc, routeParams) {
	sc.isMember 	= false;
	var tempGroup 	= null;
	sc.currentGroup = null;

	// If has params
	if (routeParams.group_id != undefined) {
		// Wants to access group with group_id
		tempGroup = DataService.getGroupWithId(parseInt(routeParams.group_id));
		if (tempGroup == null) {
			// Didn't find such group, redirect to page not found
			loc.url('/userlist');
		}
		else if (tempGroup.isPublic == false) {
			loc.url('/userlist');
		}
		else {
			sc.currentGroup = tempGroup;
			if (AuthService.currentUser() == sc.currentGroup.owner && sc.currentGroup.owner != null) {
				sc.isMember = true;
			}
			else if (DataService.isUserGroupMember(AuthService.currentUser(), sc.currentGroup)) {
				sc.isMember = true;
			}
		}
	}
	else {
		loc.url('/userlist');
	}
}]);
