'use strict'

const Promise = require('bluebird')

module.exports = {
  createTags: (text, createdBy) => {

    let tags = []

    const themeRegex = /(?:^|[^#\w])(?:#)(\w+)\b/g

    let match = themeRegex.exec(text)
    while (match != null) {
      tags.push({ type: 'theme', text: match[1] })
      match = themeRegex.exec(text)
    }

    const citeRegex = /(?:^|[^@\w])(?:@)(\w+)\b/g

    match = citeRegex.exec(text)
    while (match != null) {
      tags.push({ type: 'cite', text: match[1] })
      match = citeRegex.exec(text)
    }

    const linkRegex = /(?:\$l:")(.+?)(?:")/g

    match = linkRegex.exec(text)
    while (match != null) {
      tags.push({ type: 'link', text: match[1] })
      match = linkRegex.exec(text)
    }

    const imgRegex = /(?:\$i:")(.+?)(?:")/g

    match = imgRegex.exec(text)
    while (match != null) {
      tags.push({ type: 'image', text: match[1] })
      match = imgRegex.exec(text)
    }

    const vidRegex = /(?:\$v:")(.+?)(?:")/g

    match = vidRegex.exec(text)
    while (match != null) {
      tags.push({ type: 'video', text: match[1] })
      match = vidRegex.exec(text)
    }

    let tagIDs = []

    return User.findOne({ username: sails.config.permissions.adminUsername })
    .then(admin => {
      return Promise.map(tags, tag => {
        return Tag.create({
          owner: admin.id,
          createdBy: createdBy || admin.id,
          text: tag.text,
          type: tag.type
        })
        .then(tag => {
          tagIDs.push(tag.id)
        })
        .catch(error => { sails.log.error(error) })
      })
      .then(() => {
        return tagIDs
      })
    })
    .catch(error => { sails.log.error(error) })
  }
}
