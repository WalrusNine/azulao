'use strict'

const Promise = require('bluebird')

module.exports = sails => {

    const deps = ['hook:auth:loaded', 'hook:permissions:loaded']

    function printGrantedPermission(perm) {
        return Promise.all([
            Model.findOne({id: perm.model}),
            Role.findOne({id: perm.role})
        ])
        .spread((model, role) => {
            sails.log.info(
                `${model.name}: granted ${perm.action.toUpperCase()} to role`
                + ` "${role.name}" for ${perm.relation.toUpperCase()}`
            )
        })
    }

    function setupPublicRoleUserPermissions() {
        return Promise.all([
            Model.findOne({name: 'User'}),
            Role.findOne({name: 'public'})
        ])
        .spread((model, role) => [
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'create',
                criteria: {
                    blacklist: [
                        'id',
                        'valid',
                        'profile',
                        'createdAt',
                        'updatedAt',
                        'gravatarUrl'
                    ]
                }
            }),
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'read',
                relation: 'owner'
            }),
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'update',
                relation: 'owner',
                criteria: {
                    blacklist: [
                        'id',
                        'owner',
                        'valid',
                        'profile',
                        'createdBy',
                        'createdAt',
                        'updatedAt',
                        'gravatarUrl'
                    ]
                }
            }),
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'delete',
                relation: 'owner'
            })
        ])
        .map(printGrantedPermission)
    }

    function setupPublicRoleProfilePermissions() {
        return Promise.all([
            Model.findOne({name: 'Profile'}),
            Role.findOne({name: 'public'})
        ])
        .spread((model, role) => [
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'read',
                criteria: {
                    where: { isBirthdayPublic: false },
                    blacklist: [ 'isBirthdayPublic', 'birthday' ]
                }
            }),
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'read',
                criteria: {
                    where: { isBirthdayPublic: true },
                    blacklist: [ 'isBirthdayPublic' ]
                }
            }),
        ])
        .map(printGrantedPermission)
    }

    function setupRegisteredRoleProfilePermissions() {
        return Promise.all([
            Model.findOne({name: 'Profile'}),
            Role.findOne({name: 'registered'})
        ])
        .spread((model, role) => Promise.all([
                Permission.destroy({
                    model: model.id,
                    role: role.id,
                    action: 'read'
                }),
                Permission.destroy({
                    model: model.id,
                    role: role.id,
                    action: 'create'
                })
            ]).then(() => [model, role])
        )
        .spread((model, role) => [
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'create',
                criteria: {
                    blacklist: [
                        'id',
                        'createdAt',
                        'updatedAt',
                    ]
                }
            }),
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'read',
                relation: 'owner'
            }),
            Permission.create({
                model: model.id,
                role: role.id,
                action: 'update',
                relation: 'owner',
                criteria: {
                    blacklist: [
                        'id',
                        'owner',
                        'createdBy',
                        'createdAt',
                        'updatedAt'
                    ]
                }
            }),
        ])
        .map(printGrantedPermission)
    }

    function setupAnonUser() {
        return Promise.all([
            sails.models.user.findOne({ username: 'anon' })
            .then(user => {
                if (user) return user
                sails.log.info('azulão: anon user does not exist; creating...')
                return sails.models.user.create({
                    username: 'Anonymous',
                    email: 'anon@anonymous.net'
                })
            }),
            sails.models.user.findOne({
                username: sails.config.permissions.adminUsername
            })
        ])
        .spread((anon, admin) => {
            if (!admin) {
                sails.log.error('azulão: admin user not found!')
            }
            else {
                anon.createdBy = admin.id
                anon.owner = admin.id
                sails.log.info('azulão: created anon user:', anon)
                anon.save()
            }
        })
    }

    function setupAdminValidation() {
        return sails.models.user.findOne({
            username: sails.config.permissions.adminUsername
        })
        .then(admin => {
            if (!admin) {
                sails.log.error('azulão: admin user not found!')
            }
            else {
                admin.valid = true
                admin.save()
            }
        })
    }

    return {
        initialize: next => sails.after(deps, () => {
            return Promise.all([
                setupAnonUser(),
                setupAdminValidation(),
                setupPublicRoleUserPermissions(),
                setupPublicRoleProfilePermissions(),
                setupRegisteredRoleProfilePermissions()
            ])
            .then(() => next())
            .catch(error => { sails.log.error(error) })
        })
    }
}
