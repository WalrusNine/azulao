'use strict';

var userl = angular.module('azulao.mygrouplist', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'xeditable', 'zumba.angular-waypoints', 'auth', 'data']);

userl.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/mygrouplist', {
    templateUrl: 'angular/mygrouplist/mygrouplist.html',
    controller: 'MyGroupListCtrl'
  });
}]);

userl.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});

userl.controller('MyGroupListCtrl', ['$scope', 'AuthService', 'DataService', '$location', function(sc, AuthService, DataService, loc) {
	if (AuthService.currentUser() == null) {
		loc.url('/login/');
	}
	else {

		sc.grouplist = AuthService.currentUser().grouplist;
		sc.searchGroup = "";

		sc.goToGroupPage = function (g) {
			loc.url('/group/' + g.groupid);
		};

		sc.goToUserPage = function (u) {
			if (u != null) {
				if (u == AuthService.currentUser())
					loc.url('/perfil/')
				else
					loc.url('/user/' + u.username);
			}
		};
	}
}]);
