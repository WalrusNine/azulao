'use strict';

var login = angular.module('azulao.login', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'auth', 'data']);

login.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'angular/login/login.html',
    controller: 'LoginCtrl'
  });
}]);

login.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});


login.controller('LoginCtrl', ['$scope', 'AuthService', 'DataService', function(sc, AuthService, DataService){
	sc.logUser = new User();
	sc.regUser = new User();

	sc.isLoggingIn = true;



	sc.login = function() {
		AuthService.login(sc.logUser);
	};

	sc.cancel = function () {
		sc.isLoggingIn = true;
	};

	sc.register = function () {
		// Create and add user
		var u = DataService.createUser(sc.regUser.displayname, sc.regUser.username, sc.regUser.password, sc.regUser.email, sc.regUser.birthdate);
		DataService.addUser(u);

		// Set login's username field to newly created user's
		sc.logUser.username = sc.regUser.username;

		// Clear form
		sc.regUser.displayname 	= "";
		sc.regUser.username 	= "";
		sc.regUser.password 	= "";
		sc.regUser.email 		= "";
		sc.regUser.birthdate 	= new Date();

		// Go to login
		sc.isLoggingIn = true;
	};

	sc.setIsLoggingIn = function (b) {
		sc.isLoggingIn = b;
	};

}]);
