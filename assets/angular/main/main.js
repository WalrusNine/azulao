'use strict';

var perfil = angular.module('azulao.main', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'xeditable', 'zumba.angular-waypoints', 'auth', 'data', 'lazy-scroll']);

perfil.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/main', {
		templateUrl: 'angular/main/main.html',
		controller: 'MainCtrl'
	});
}]);

perfil.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});

perfil.controller('MainProfileCtrl', ['$scope', 'AuthService', 'DataService', '$uibModal', function(sc, AuthService, DataService, modal) {
	// Temp classes as models to view
	sc.currentUser = AuthService.currentUser();
	// Watch for current user
	sc.$watch(AuthService.isLoggedIn, function(isLoggedIn) {
		if (!isLoggedIn) {
			// Logged out
			sc.currentUser = null;
		}
		else {
			// Logged in
			sc.currentUser = AuthService.currentUser();
		}
	});

}]);

perfil.controller('MainPostCtrl', ['$scope', 'AuthService', 'DataService', function(sc, AuthService, DataService) {
	// In this page, posts are automatically added to user's postlist
	sc.tempPost = new Post();

	sc.addPost = function() {
		// Create post
		var p = DataService.createPost(AuthService.currentUser(), sc.tempPost.title, sc.tempPost.content, null);

		// Add post
		DataService.addPost(p);

		// Reset dummy variable
		sc.tempPost.title = "";
		sc.tempPost.content = "";
	};

}]);

perfil.controller('MainPostListCtrl', ['$scope', 'AuthService', 'DataService', '$location', function(sc, AuthService, DataService, loc) {
	// Shows user's posts, followed users' posts and groups' posts
	sc.currentUser = AuthService.currentUser();

	if(sc.currentUser !== null) {

		// Posts currently showing to user
		sc.posts = [];
		var postsIndex 		= 0;
		var myPostsIndex 	= 0;
		var groupsPostsIndex = [];
		var followedUsersPostsIndex = [];

		// Initialize all
		for (var i = 0; i < sc.currentUser.followedusers.length; ++i) {
			followedUsersPostsIndex.push(0);
		}
		for (i = 0; i < sc.currentUser.grouplist.length; ++i) {
			groupsPostsIndex.push(0);
		}


		// Gets n posts
		sc.getPost = function(n) {
			// Get n posts from any list
			// Indices keep track of where last posts were from
			// Compare posts by date and get most recent
			var tempDate = new Date();
			for (var ii = 0; ii < n; ++ii) {
				// First get from user's post list
				var userPost = new Post();
				userPost = sc.currentUser.postlist[myPostsIndex];

				// Then get from group's post list
				var groupPost = new Post();
				groupPost.timestamp = tempDate;
				groupPost.title = "$$deleteg";
				var tempGroupIndex = -1;

				for (var i = 0; i < sc.currentUser.grouplist.length; ++i) {
					var group = sc.currentUser.grouplist[i];
					if (groupPost.title === "$$deleteg") {
						// If there's no post found, set current as the one
						var p = group.postlist[groupsPostsIndex[i]];
						if (p != null) {
							groupPost = p;
							tempGroupIndex = i;
						}
					}
					else {
						// Else, compare with already found one
						if (group.postlist[groupsPostsIndex[i]] !== undefined) {
							if (groupPost.timestamp.getTime() <= group.postlist[groupsPostsIndex[i]].timestamp.getTime()) {
								// Current group has an earlier post
								groupPost = group.postlist[groupsPostsIndex[i]];
								tempGroupIndex = i;
							}
						}
					}
				}

				// Then get from following's post list
				var followPost = new Post();
				followPost.timestamp = tempDate;
				followPost.title = "$$deletef";
				var tempFollowIndex = -1;
				for (var i = 0; i < sc.currentUser.followedusers.length; ++i) {
					var follow = sc.currentUser.followedusers[i];
					if (followPost.title === "$$deletef") {
						// If there's no post found, set current as the one
						var p = follow.postlist[followedUsersPostsIndex[i]];
						if (p != null) {
							followPost = p;
							tempFollowIndex = i;
						}
					}
					else {
						// Else, compare with already found one
						if (follow.postlist[followedUsersPostsIndex[i]] !== undefined) {
							if (groupPost.timestamp.getTime() >= follow.postlist[followedUsersPostsIndex[i]].timestamp.getTime()) {
								// Current follow has an earlier post
								followPost = follow.postlist[followedUsersPostsIndex[i]];
								tempFollowIndex = i;
							}
						}
					}

				}

				// Get earliest
				if (userPost === undefined){
					userPost = new Post();
					userPost.timestamp = new Date();
					userPost.title = "$$deleteu";
				}
				// Check if are all "$$delete"
				if (userPost.title === "$$deleteu" && groupPost.title === "$$deleteg" && followPost.title === "$$deletef") {
					console.log("return");
					return;
				}

				if (groupPost.timestamp.getTime() > userPost.timestamp.getTime() && groupPost.title !== "$$deleteg") {
					// group post is earlies than user post
					if (groupPost.timestamp.getTime() > followPost.timestamp.getTime()) {
						sc.posts.push(groupPost);
						groupsPostsIndex[tempGroupIndex]++;
					}
					else if (followPost.title !== "$$deletef"){
						sc.posts.push(followPost);
						followedUsersPostsIndex[tempFollowIndex]++;
					}
					else {
						sc.posts.push(groupPost);
						groupsPostsIndex[tempGroupIndex]++;
					}
				}
				else {
					// user post is earlier than group post
					if (userPost.timestamp.getTime() > followPost.timestamp.getTime() && userPost.title !== "$$deleteu") {
						sc.posts.push(userPost);
						myPostsIndex++;
					}
					else if (followPost.title !== "$$deletef") {
						sc.posts.push(followPost);
						followedUsersPostsIndex[tempFollowIndex]++;
					}
					else {
						sc.posts.push(userPost);
						myPostsIndex++;
					}
				}

			}




		};

		// Watch user's postlist
		/*sc.$watchCollection("currentUser.postlist", function(newValue, oldValue) {
			if (sc.currentUser.postlist[0] !== undefined) {
				myPostsIndex++;
				console.log(sc.currentUser.postlist[0].content);
				sc.posts.unshift(sc.currentUser.postlist[0]);
			}
		});

		// Watch followed users' groups
		for (i = 0; i < sc.currentUser.grouplist.length; ++i) {
			sc.$watchCollection("currentUser.grouplist[i].postlist", function(newValue, oldValue) {
				if (sc.currentUser.grouplist[i].postlist !== undefined) {
					groupsPostsIndex[i]++;
					sc.posts.unshift(sc.currentUser.grouplist[i].postlist[0]);
				}
			});
		}
		// Watch followed users' groups
		for (i = 0; i < sc.currentUser.followedusers.length; ++i) {
			sc.$watchCollection("currentUser.followedusers[i].postlist", function(newValue, oldValue) {
				if (sc.currentUser.followedUsers[i].postlist !== undefined) {
					followedUsersPostsIndex[i]++;
					console.log(sc.currentUser.followedusers[i].postlist[0].content);
					sc.posts.unshift(sc.currentUser.followedusers[i].postlist[0]);
				}
			});
		}*/
		// Infinite scroll
		sc.reachEndEvent = function() {
			console.log("Reach end");
			sc.getPost(5);
		};

		// Get puser's inital posts
		sc.getPost(5);

		sc.goToUserPage = function(user) {
			if (user != sc.currentUser)
				loc.url('/user/' + user.username);
		};

		sc.goToGroupPage = function(g) {
			loc.url('/group/' + g.groupid);
		};
	}

}]);

perfil.controller('MainCtrl', ['$scope', 'AuthService', 'DataService', '$location', '$routeParams', function(sc, AuthService, DataService, loc, routeParams) {
	// In this page the user can see not only its own posts but all group and following users' posts
	if (AuthService.currentUser() === null) {
		loc.url('/login/');
	}
}]);
