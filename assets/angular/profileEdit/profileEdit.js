'use strict';

var pEdit = angular.module('azulao.profileEdit', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'auth', 'data']);

pEdit.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});


pEdit.controller('ProfileEditCtrl', ['$scope', 'AuthService', 'DataService', '$uibModalInstance', '$uibModal', function(sc, AuthService, DataService, modal, nmodal){
	sc.currentUser = AuthService.currentUser();
	sc.user 		= {};
	sc.tempGroup 	= new Group();

	//DataService.copyUserInto(AuthService.currentUser(), sc.user);
	sc.user.username = sc.currentUser.username;
	sc.user.email = sc.currentUser.email;

	sc.done = function() {
		//DataService.copyUserInto(sc.user, AuthService.currentUser());
		DataService.updateUser(AuthService.currentUser(), sc.user).then(
			function(result) {
				if (result != null) {
					modal.close();
				}
				else {
					console.log("Failed to update user.");
				}
			}
		);

	};

	sc.cancel = function () {
		modal.dismiss();
	};

	sc.deleteAccount = function () {
		DataService.removeUser(AuthService.currentUser());
		AuthService.logout();
		modal.close();
	};

	sc.editGroup = function (g) {
		// Is editing
		var m = nmodal.open({
			animation: true,
			templateUrl: 'angular/groupEdit/groupEdit.html',
			controller: 'GroupEditCtrl',
			resolve: {
				editingGroup: function () {
					return g;
				}
			}
		});
	};

	sc.deleteGroup = function(g) {
		DataService.removeGroup(g);
	};

	sc.addGroup = function() {
		DataService.addGroup(DataService.createGroup(sc.tempGroup.groupname, "No description."), AuthService.currentUser());
	};

	sc.removeFromGroup = function(u, g) {
		DataService.removeUserFromGroup(u, g);
	};

}]);
