'use strict';

var gEdit = angular.module('azulao.groupEdit', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'auth', 'data']);

gEdit.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});


gEdit.controller('GroupEditCtrl', ['$scope', 'AuthService', 'DataService', '$uibModalInstance', 'editingGroup', function(sc, AuthService, DataService, modal, editingGroup){
	sc.currentUser 	= AuthService.currentUser();
	sc.editingGroup = editingGroup;
	sc.tempAddUser 	= new User();
	sc.tempGroup 	= new Group();
	
	sc.tempGroup.groupname 		= sc.editingGroup.groupname;
	sc.tempGroup.description 	= sc.editingGroup.description;
	
	sc.done = function() {
		sc.editingGroup.groupname 	= sc.tempGroup.groupname;
		sc.editingGroup.description = sc.tempGroup.description;
		modal.close();
	};
	
	sc.cancel = function () {
		modal.dismiss();
	};
	
	sc.deleteGroup = function () {
		DataService.removeGroup(sc.editingGroup);
		AuthService.setEditingGroup(null);
		modal.close();
	};
	
	sc.removeFromGroup = function(u, g) {
		DataService.removeUserFromGroup(u, g);
	};
	
	sc.addUserToGroup = function() {
		DataService.addUserToGroup(sc.tempAddUser, sc.editingGroup);
	};
	
}]);
