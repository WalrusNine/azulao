'use strict';

var layout = angular.module('azulao.layout', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'xeditable', 'auth', 'data']);


layout.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});

layout.controller('LayoutCtrl', ['$scope', 'AuthService', 'DataService', '$location', function(sc, AuthService, DataService, loc){
	sc.selected = "";
	sc.userlist = DataService.getUsers();
	sc.isUserLoggedIn = AuthService.isLoggedIn();

	sc.signOut = function () {
		AuthService.logout();
	};

	sc.isActive = function (viewLocation) {
		return viewLocation == loc.path();
	}

	// Watch for current user
    sc.$watch(AuthService.isLoggedIn, function(isLoggedIn) {
		sc.isUserLoggedIn = isLoggedIn;
		if (isLoggedIn) {
			// Is logged in, redirect to perfil
			loc.url("/perfil");
		}
		else {
			// Is logged out, redirect to login
			loc.url("/login");
		}
	});

}]);
