/**
* UserController
*
* @description :: Server-side logic for managing users
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

'use strict'

const _ = require('lodash')

_.merge(exports, require('sails-auth/dist/api/controllers/UserController'))
_.merge(exports, {

    validate: (req, res) => {
        const id = req.param('id')
        User.findOne({id: id})
        .then(user =>
            PermissionService.addUsersToRole(user.username, 'registered')
        )
        .catch(error => { sails.log.error(error) })
        return res.redirect('/')
    }
})
