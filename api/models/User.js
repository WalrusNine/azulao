/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
*/
'use strict'

const _ = require('lodash')

_.merge(exports, require('sails-permissions/dist/api/models/User'))
_.merge(exports, {

  attributes: {
    profile: {
      collection: 'profile',
      via: 'owner'
    },
    groups: {
      collection: 'group',
      via: 'users',
      dominant: true
    },
    posts: {
      collection: 'post',
      via: 'id'
    },
    follows: {
      collection: 'user',
      via: 'followedBy'
    },
    followedBy: {
      collection: 'user',
      via: 'follows'
    },
    likes: {
      collection: 'post',
      via: 'likedBy'
    }
  },

  afterCreate: [
    (user, next) => {
      sails.log.verbose('User.afterCreate.setOwner', user)
      User.update({ id: user.id }, { owner: user.id })
      .then(user => {
        next()
      }).catch(e => {
        sails.log.error(e)
        next(e)
      })
    },
    (user, next) => {
      sails.log.verbose('User.afterCreate.attachDefaultRole', user)
      User.findOne(user.id).populate('roles')
      .then(_user => {
        user = _user
        return Role.findOne({ name: 'public' })
      })
      .then(role => {
        user.roles.add(role.id)
        return user.save()
      })
      .then(updatedUser => {
        sails.log.silly('role "public" attached to user', user.username)
        next()
      })
      .catch(e => {
        sails.log.error(e)
        next(e)
      })
    }
  ]
})
