/**
 * Post.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    title: {
      type: 'string'
    },
    location: {
      type: 'string'
    },
    text: {
      type: 'string',
      required: true
    },
    likedBy: {
      collection: 'user',
      via: 'likes'
    },
    tags: {
      collection: 'tag',
      via: 'usedIn'
    }
  }
};
