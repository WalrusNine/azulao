/**
* sessionAuth
*
* @module      :: Policy
* @description :: Simple policy to allow any authenticated user
*                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
* @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
*
*/
'use strict'

module.exports = (req, res, next) => {

    // User is allowed, proceed to the next policy,
    // or if this is the last policy, the controller
    if (req.session.authenticated) {
        return next()
    } else {
        User.findOne({username: 'Anonymous'})
        .then(user => {
            req.user = user
            return next()
        })
    }
}
