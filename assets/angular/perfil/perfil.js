'use strict';

var perfil = angular.module('azulao.perfil', ['ngRoute', 'ui.bootstrap', 'ngAnimate', 'xeditable', 'zumba.angular-waypoints', 'auth', 'data', 'lazy-scroll']);

perfil.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/perfil', {
		templateUrl: 'angular/perfil/perfil.html',
		controller: 'PerfilCtrl'
	});
	$routeProvider.when('/user/:user_id', {
		templateUrl: 'angular/perfil/perfil.html',
		controller: 'PerfilCtrl'
	});
}]);

perfil.run(function(editableOptions) {
	editableOptions.theme = "bs3";
});

perfil.controller('ProfileCtrl', ['$scope', 'AuthService', 'DataService', '$uibModal', function(sc, AuthService, DataService, modal) {
	if (!sc.isForeign) {
		// Temp classes as models to view
		//sc.currentUser = AuthService.currentUser();
		// Watch for current user
		sc.$watch(AuthService.isLoggedIn, function(isLoggedIn) {
			if (!isLoggedIn) {
				// Logged out
				sc.currentUser = null;
			}
			else {
				// Logged in
				sc.currentUser = AuthService.currentUser();
			}
		});
	}

	sc.editProfile = function (bool) {
		if (bool) {
			// Is editing
			var m = modal.open({
				animation: true,
				templateUrl: 'angular/profileEdit/profileEdit.html',
				controller: 'ProfileEditCtrl'
			});
		}
	};

	sc.follow = function () {
		// currentUser follows puser
		DataService.followUser(AuthService.currentUser(), sc.puser);
		// ERROR: sc.currentUser and sc.puser are the same
		console.log(AuthService.currentUser() + " is now following " + sc.puser.displayname);
	}

}]);

perfil.controller('PostCtrl', ['$scope', 'AuthService', 'DataService', function(sc, AuthService, DataService) {
	sc.tempPost = new Post();

	sc.addPost = function() {
		// Create post
		var p = DataService.createPost(AuthService.currentUser(), sc.tempPost.title, sc.tempPost.content, null);
		// Add post
		DataService.addPost(p).then(
			function(result) {
				if (result != null) {
					// Success
					// Add post p
					sc.newPosts.unshift(result);
				}
			}

		);

		// Reset dummy variable
		sc.tempPost.title = "";
		sc.tempPost.content = "";
	};

}]);

perfil.controller('PostListCtrl', ['$scope', 'AuthService', 'DataService', '$location', function(sc, AuthService, DataService, loc) {
	sc.listOwner = sc.isForeign ? sc.puser : AuthService.currentUser();
	sc.ownerPostList = null;

	if (sc.listOwner != null) {
		DataService.getUserPosts(sc.listOwner.id).then(function(result) {
			if (result != null) {
				sc.ownerPostList = result;

				// Posts currently showing to user
				sc.posts = [];
				sc.postsIndex = 0;

				// Gets n posts
				sc.getPost = function(n) {
					// sc.posts is a subarray of sc.currentUser.postlist
					var p = sc.postsIndex;
					sc.postsIndex += n;

					if (sc.postsIndex > sc.ownerPostList.length)
						sc.postsIndex = sc.ownerPostList.length;

					sc.posts = sc.ownerPostList.slice(0, sc.postsIndex);
					sc.posts.reverse();
					sc.posts = sc.newPosts.concat(sc.posts);
				};

				if (sc.isForeign == false) {
					// Watch for current user
					sc.$watch(AuthService.isLoggedIn, function(isLoggedIn) {
						if (!isLoggedIn) {
							// Logged out/Deleted Account
							sc.postsIndex = 0;
							sc.posts = [];
							// Or go to login page? Or stay but as foreign?
						}
						else {
							// Logged in, get initial posts
							sc.postsIndex = 0;
							sc.getPost(5);
						}
					});
				}
				else {
					// Get puser's inital posts
					sc.getPost(5);
				}

				sc.$watchCollection("newPosts", function(newValue, oldValue) {
					sc.getPost(1);
				});

				sc.reachEndEvent = function() {
					sc.getPost(5);
				};

				sc.goToUserPage = function(user) {
					if (user != sc.listOwner)
						loc.url('/user/' + user.username);
				};

				sc.goToGroupPage = function(g) {
					loc.url('/group/' + g.groupid);
				};
			} else {
				// Error
				console.log("Failed to load post list of user: " + sc.listOwner.username);
			}
		});
	}

}]);

perfil.controller('PerfilCtrl', ['$scope', 'AuthService', 'DataService', '$location', '$routeParams', function(sc, AuthService, DataService, loc, routeParams) {
	sc.isForeign = false;
	sc.puser = null;
	sc.newPosts = [];
	// If has params
	if (routeParams.user_id != undefined) {
		// Check if it's current user's profile
		{
			var c = AuthService.currentUser();
			if (c != null && c.username == routeParams.user_id)
				loc.url("/perfil");
		}
		sc.isForeign = true;
		// Wants to access someone's profile
		DataService.getUserWithUsername(routeParams.user_id).then(
			function(result) {
				sc.puser = result[0];
				if (sc.puser != null) {
					// If is valid username
					sc.isForeign = true;
				}
				else {
					// If isn't valid username, error
					// Go to own profile
					loc.url("/perfil");
				}
			}
		);
	}
	else {
		// Wants to access own profile?
		if (AuthService.isLoggedIn()) {
			sc.isForeign = false;
			sc.puser = AuthService.currentUser();
		}
		else {
			// If isn't logged in, go to log in page
			loc.url("/login");
		}
	}
}]);
