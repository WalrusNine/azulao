'use strict'

const _ = require('lodash')

_.merge(exports, require('sails-permissions/dist/api/services/PermissionService'))
_.merge(exports, {

    getErrorMessage: options => {
        return [options.user.username, 'is not permitted to',
            options.method, options.model.name].join(' ')
    },
})
