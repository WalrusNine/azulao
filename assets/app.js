'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('azulao', [
    'ngRoute',
    'azulao.perfil',
    'azulao.layout',
    'azulao.userlist',
    'azulao.login',
    'azulao.groupEdit',
    'azulao.profileEdit',
    'azulao.group',
    'azulao.mygrouplist',
    'azulao.main',
    'auth',
    'data'
]);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/login'});
}]);

var auth = angular.module('auth', []);

auth.factory("AuthService", function($http){
	var currentUser = null;

	return {
		login: function(user) {
      $http.post('/auth/local', {
        identifier: user.username || user.email,
        password: user.password
      }).then(function (response) {
        currentUser = response.data;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not login.");
      });
    },
		logout: function(){
      $http.get(
        '/logout'
      ).then(function (response) {
        currentUser = null;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not logout.");
      });

    },
		isLoggedIn: function() {
      return currentUser != null;
    },
		currentUser: function() {
      return currentUser;
    }
	};
});

var data = angular.module('data', []);

function User() {
	this.username;		// Unique
	this.password;
	this.email;
	this.displayname;	// Not unique
	this.photo;
	this.description = "No description";
	this.birthdate = new Date();
	this.postlist = [];
	this.grouplist = [];
	this.followedusers = [];
};

function Post() {
	this.user;
	this.title;
	this.group;		// If the user seeing the post wants to go to group's page, iterate through his group list
	this.content;
	this.timestamp = new Date();
	this.location;
};

function Group() {
	this.owner;
	this.groupname;		//Unique
	this.description;
	this.userlist = [];
	this.postlist = [];
	this.groupid;		// Although groupname is unique, owner can change it. Group id is the key.
	this.isPublic;
};

Post.formatDate = function(date) {
	return date.getDate() + "/" + (date.getMonth()+1)
    	+ "/" + date.getFullYear() + " " + date.getHours() + ":"
    	+ date.getMinutes() + ":" + date.getSeconds();
}

/*
 * Group has list of users.
 * User has list of groups.
 * Data has list of users and groups.
 *
 *
 * */
data.factory("DataService", function($http) {

	var createDemoUser = function() {
		// Create example user
		var user = new User();

		user.displayname 	= "Nome Sobrenome";
		user.username 		= "myusername";
		user.description 	= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a ex vestibulum, dictum eros at, bibendum enim. Sed ultrices viverra consectetur. Nunc a eleifend dui. Interdum et malesuada fames ac.";
		user.photo 			= "http://www.gravatar.com/avatar/?d=identicon&s=200";
		user.email 			= "myemail@gmail.com";
		user.password 		= "mypassword";

		var g 		= new Group();
		g.owner 	= user;
		g.groupname = "Cool Group";
		g.groupid 	= 1;
		g.description = "Through being cool";
		g.isPublic = true;
		grouplist.push(g);
		g.userlist.push(user);
		user.grouplist.push(g);

		g = new Group();
		g.owner = null;
		g.groupname = "Family";
		g.groupid = 2;
		g.description = "We are family";
		g.isPublic = true;
		grouplist.push(g);
		g.userlist.push(user);
		user.grouplist.push(g);

		for (var i = 0; i < 20; ++i) {
			var post 		= new Post();
			var currentDate = new Date();

			post.user 		= user;
			post.title 		= "Title " + i;
			post.content 	= "Content " + i;
			// TODO: leave timestamp as Date var, format only to show
			post.timestamp 	= currentDate;
            post.timestamp.setTime(post.timestamp.getTime() + i*60000);

			post.location = "São Paulo";

			if (i % 2) {
				post.group = g;
				g.postlist.unshift(post);
			}
            else {
			    // Add
			    user.postlist.unshift(post);
            }
		}

		return user;
	};

  var indexFromID = function (list, id) {
    var index = -1;
    for (var i = 0; i < list.length; ++i) {
      if (list[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  };

	return {

		addUser: function(user) {
			return $http.post('/user', {
        username: user.username,
        email: user.email,
        password: user.password
      }).then(function (response) {
          return response.data;
      }).catch(function (response) {
          console.error("Server error " + response.status
            + ": " + response.statusText
            + ": could not add user.");
      });
		},

        updateUser: function(user, newUserData) {
			return $http.put('/user/' + user.id, {
        username: newUserData.username,
        email: newUserData.email
        //password: newUserData.password
      }).then(function (response) {
          user.username = newUserData.username;
          user.email = newUserData.email;
          return response.data;
      }).catch(function (response) {
          console.error("Server error " + response.status
            + ": " + response.statusText
            + ": could not update user.");
      });
		},

		removeUser: function(user) {
      return $http.delete('/user/' + user.id)
      .then(function (response) {
        return response.data;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not remove user.");
      });
		},

		addGroup: function(group, owner) {
      return $http.post('/group', {
        name: group.name,
        description: group.description,
        users: [owner.id]
      }).then(function (response) {
          return response.data;
      }).catch(function (response) {
          console.error("Server error " + response.status
            + ": " + response.statusText
            + ": could not add group.");
      });
		},

		addFollowedUser: function(user, follow) {
			return $http.post(
        '/user/' + user.id + "/follows/" + follow.id
      ).then(function (response) {
          return response.data;
      }).catch(function (response) {
          console.error("Server error " + response.status
            + ": " + response.statusText
            + ": could not add followed user.");
      });
		},

		removeGroup: function(group) {
			return $http.delete(
        '/group/' + group.id
      ).then(function (response) {
        return response.data;
      }).catch(function (response) {
        console.log("Server error " + response.status
          + ": " + response.statusText
          + ": could not remove group.");
      });
		},

		removeUserFromGroup: function(user, group) {
			return $http.delete(
        '/group/' + group.id + '/users/' + user.id
      ).then(function (response) {
          return response.data;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not remove user from group.");
      });
		},

		addUserToGroup: function(user, group) {
      return $http.post(
        '/group/' + group.id + '/users/' + user.id
      ).then(function (response) {
          return response.data;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not add user to group.");
      });
		},

		createGroup: function(groupname, groupdescription) {
			return {name: groupname, description: groupdescription};
		},

		getGroupWithId: function(id) {
			return $http.get(
        '/group/' + id
      ).then(function (response) {
        return response.data;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not find group with id " + id + " .");
      });
		},

		getUserWithUsername: function(username) {
			return $http.get(
        '/user?username=' + username
      ).then(function (response) {
        return response.data;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not find user with username " + username + " .");
      })
		},

		createUser: function(displayname, username, password, email, bd) {
			var u = new User();

			u.username 		= username;
			u.password 		= password;
			u.email 		= email;
			u.displayname 	= displayname;
			u.birthdate 	= bd;

			return u;
		},

		addPost: function(post) {
			return $http.post('/post', {
        title: post.title,
        text: post.content,
        location: post.location,
        owner: post.user
      }).then(function (response) {
        return response.data;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not create post.");
      })
		},

		createPost: function(user, title, content, group) {
			var p = new Post();

			p.title 	= title;
			p.content 	= content;
			p.user 		= user;
			p.timestamp = Post.formatDate(new Date());
			p.location 	= "São Paulo";
			p.group 	= group;

			return p;
		},

		copyUserInto: function(source, dest){
			dest.displayname = source.displayname;
			dest.username 	= source.username;
			dest.password 	= source.password;
			dest.birthdate 	= source.birthdate;
			dest.photo 		= source.photo;
			dest.description = source.description;
			dest.email		= source.email;
		},

		isUserGroupMember: function(user, group) {
			return $http.get(
        '/user/' + user.id + '/groups'
      ).then(function (response) {
        var groupIndex = indexFromID(response.data, group.id);
        return groupIndex >= 0;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not query user groups.");
      });
		},

		followUser: function (user, userToFollow) {
			return $http.post(
        '/user/' + user.id + '/follows/' + userToFollow.id
      ).then(function (response) {
        return response.data;
      }).catch(function (response) {
        console.error("Server error " + response.status
          + ": " + response.statusText
          + ": could not follow user.");
      });
		},

        getUsers: function () {
          return $http.get(
            '/user'
          ).then(function (response) {
              console.log(response);
              return response.data;
          }).catch(function (response) {
              console.error("Server error " + response.status
                + ": " + response.statusText
                + ": could not get users.");
          });
        },

        getUserPosts: function (id) {
            return $http.get(
                '/post?owner=' + id
            ).then(function(response){
                return response.data;
            }).catch(function(response){
                console.error("Server error " + response.status
                  + ": " + response.statusText
                  + ": could not get user's posts.");
            });
        }

	};
});
