/**
* Profile.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
*/

module.exports = {

  attributes: {
    owner: {
      model: 'user',
      unique: true
    },
    displayname: {
      type: 'string',
      maxLength: 64
    },
    avatarURL: {
      type: 'string',
      url: true
    },
    about: {
      type: 'string',
      maxLength: 500
    },
    birthday: {
      type: 'date',
      after: '1/1/1900'
    },
    isBirthdayPublic: {
      type: 'boolean',
      defaultsTo: false
    }
  }
};
